from kivy.core.window import Window
import settings


def go_to_screen(*args):
    screen = args[0]
    a = args[1:]
    settings.runtime.get('sm').switch_to(screen(*a))


def update_rect(instance, value):
    instance.rect.pos = instance.pos
    instance.rect.size = instance.size


def store_download(task):
    settings.runtime.get('downloads').append(task)


def interpret_download_status(status_code):
    return {
        1: 'Pending',
        2: 'Running',
        4: 'Paused',
        8: 'Successful',
        16: 'Failed',
    }.get(status_code)


def density_value(v):
    return Window.width / v
