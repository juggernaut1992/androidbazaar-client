

class Comment:
    text = None
    author = None
    created = None
    positive_reactions = 0
    negative_reactions = 0

    @classmethod
    def from_json(cls, **kwargs):

        instance = cls()

        for k, v in kwargs.items():
            instance.__setattr__(k, v)
        return instance
