from network.user import register, login


class User:
    user_name = None
    # bio = None
    # display_name = None
    email_address = None
    session = None

    @classmethod
    def from_json(cls, *args):
        instance = cls()
        # FIXME: what the fuck?!
        # for k, v in payload.items():
        #     instance.k = v
        return instance

    @classmethod
    def new(cls, username, password, email):
        params_are_valid, error = cls._validate_params(username, password, email)
        if params_are_valid:
            register(username, password, email)
            return True

    @classmethod
    def get_me(cls, username, password):
        login(username, password, cls.from_json)

    @staticmethod
    def _validate_params(username, password, email):
        # TODO: validate email
        if len(username) < 4:
            return False, 'username must be at least 4 characters'
        if len(password) < 8:
            return False, 'password must be at least 8 characters'
        return True, None
