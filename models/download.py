from os import path

from kivy.uix.progressbar import ProgressBar
from kivy.utils import platform

import settings


if platform == 'android':
    from jnius import cast, autoclass
    Intent = autoclass('android.content.Intent')
    Uri = autoclass('android.net.Uri')
    if hasattr(settings, 'runtime') and settings.runtime.get('sdk') > 22:
        Service = autoclass('org.kivy.android.PythonActivity').mActivity
    else:
        Service = autoclass('org.kivy.android.PythonActivity')
    Context = autoclass('android.content.Context')


class DownloadTask(object):
    def __init__(self, url, headers, app_uid, package_name):
        self.url = url
        self.headers = headers
        self.progress_bar = ProgressBar()
        self.app_uid = app_uid
        self.package_name = package_name
        self.status = None
        self.android_download_uid = None
        self.path = self._make_path()

    def start(self):
        DownloadManager = Service.getSystemService(Context.DOWNLOAD_SERVICE)
        Request = autoclass('android.app.DownloadManager$Request')
        request = Request(Uri.parse(self.url))
        request.setDestinationInExternalPublicDir(self._make_path(), 'KingoRoot.apk')

        if settings.runtime.get('sdk') > 13:
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

        return DownloadManager.enqueue(request)

    def _make_path(self):
        Environment = autoclass('android.os.Environment')
        return Environment.DIRECTORY_DOWNLOADS
