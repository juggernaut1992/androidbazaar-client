

class Application:
    label = None
    package_name = None
    version = None
    size = None
    description = None
    changelog = None
    thumbnails = None
    logo = None

    @classmethod
    def from_json(cls, **kwargs):
        instance = cls()

        for k, v in kwargs.items():
            instance.k = v

        return instance
