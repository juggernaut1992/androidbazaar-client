from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.button import Button
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.progressbar import ProgressBar
from kivy.core.window import Window
from kivy.utils import platform
from kivy.metrics import dp

from helpers import interpret_download_status
import settings

if platform == 'android':
    from jnius import cast, autoclass
    Intent = autoclass('android.content.Intent')
    Uri = autoclass('android.net.Uri')
    if hasattr(settings, 'runtime') and settings.runtime.get('sdk') > 22:
        Service = autoclass('org.kivy.android.PythonActivity').mActivity
    else:
        Service = autoclass('org.kivy.android.PythonActivity')
    Context = autoclass('android.content.Context')


class DownloadContainer(RelativeLayout):
    def __init__(self, task):
        super(DownloadContainer, self).__init__()
        self.size_hint = None, None
        self.size = Window.width, Window.height / 10
        with self.canvas.before:
            Color(0.8, 0.99, 1, 0.9)
            self.rect = Rectangle(pos=self.pos, size=self.size)
        self.task = task
        self.app_name = self.task.package_name
        self.progress_bar = self.task.progress_bar
        self.progress_bar.size_hint = (None, 1)
        self.progress_bar.size = (Window.width / 4, Window.height)
        self.progress_bar.pos_hint = {'center_x': 0.15, 'center_y': 0.1}

        application_label = Label(
            text=self.app_name,
            color=(0, 0, 0, 0.5),
            pos_hint={'center_y': 0.5, 'center_x': 0.8},
        )

        self.status = Button(
            text=str(task.status),
            background_normal='',
            background_color=(0.9, 0.0, 0.3, 0.8),
            pos_hint={'center_x': 0.15, 'center_y': 0.5},
            size_hint=(None, None),
            size=(Window.width / 4, Window.height / 15)
        )

        self.add_widget(application_label)
        self.add_widget(self.status)
        self.add_widget(self.progress_bar)

    def update_status(self):
        DownloadManager = Service.getSystemService(Context.DOWNLOAD_SERVICE)
        Query = autoclass('android.app.DownloadManager$Query')
        query = Query()
        query.setFilterById(self.task.android_download_uid)
        cursor = DownloadManager.query(query)
        if cursor.moveToFirst():
            self.status.text = interpret_download_status(cursor.getInt(cursor.getColumnIndex('status')))


class AppContainer(RelativeLayout):

    def __init__(self, uid, name, img_src, price):
        super(AppContainer, self).__init__()

        self.size_hint = None, None
        self.size = Window.width / 4, Window.height
        self.app_uid = uid
        self.name = name
        self.img_src = img_src
        self.price = price
        app_name = Label(
            text=self.name,
            color=(1, 1, 1, 0.7),
            pos_hint={'center_y': 0.75, 'center_x': 0.5},
            font_name='Sans',
            rtl=True
        )
        app_price = Label(
            text=self.price,
            color=(1, 1, 1, 0.7),
            pos_hint={'center_y': 0.71, 'center_x': 0.5},
            font_name='Sans',
            font_size=dp(12),
            rtl=True
        )
        image = AsyncImage(
            source=self.img_src,
            width=self.width,
            height=self.height,
            allow_stretch=False,
            pos_hint={'center_y': 0.88}
        )
        self.add_widget(image)
        self.add_widget(app_name)
        self.add_widget(app_price)


class CommentContainer(RelativeLayout):

    def __init__(self, text, author, created, plus, minus):
        super(CommentContainer, self).__init__()
        self.size_hint = None, None
        self.size = Window.width, Window.height / 10
        with self.canvas.before:
            Color(0.8, 0.99, 1, 0.9)
            self.rect = Rectangle(pos=self.pos, size=self.size)

        self.text = text
        self.author = author
        self.created = created
        self.plus = plus
        self.minus = minus

        comment_author = Label(
            text=self.author,
            color=(0, 0, 0, 0.5),
            pos_hint={'center_y': 0.8, 'center_x': 0.8},
            font_name='Sans',
            rtl=True
        )

        comment_text = Label(
            text=self.text,
            color=(0, 0, 0, 1),
            pos_hint={'center_y': 0.72, 'center_x': 0.9},
            font_size=dp(14),
            size_hint=(1, None),
            font_name='Sans',
            rtl=True
        )
        comment_text.bind(width=lambda *x: comment_text.setter('text_size')(comment_text, (comment_text.width, None)))
        comment_text.bind(texture_size=lambda *x: comment_text.setter('height')(comment_text, comment_text.texture_size[1]))

        comment_created = Label(
            text=self.created,
            color=(0, 0, 0, 1),
            pos_hint={'center_y': 0.5},
            font_name='Sans',
            rtl=True
        )

        ups = Label(
            text=self.plus,
            color=(0, 1, 0, 1),
            pos_hint={'center_y': 0.72, 'center_x': 0.1},
            font_name='Sans',
            rtl=True
        )
        downs = Label(
            text=self.minus,
            color=(1, 0, 0, 1),
            pos_hint={'center_y': 0.4, 'center_x': 0.1},
            font_name='Sans',
            rtl=True
        )

        self.add_widget(comment_author)
        self.add_widget(comment_created)
        self.add_widget(comment_text)
        self.add_widget(ups)
        self.add_widget(downs)
