from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager
from kivy.app import App
from kivy.core.text import Label as CoreLabel
from kivy.utils import get_color_from_hex

import screens
from helpers import go_to_screen
import settings

CoreLabel.register(
    'Sans',
    'fonts/sans.ttf'
)
CoreLabel.register(
    'Nazanin',
    'fonts/nazanin.ttf'
)


class AndroidBazaar(App):
    sm = ScreenManager()

    def build(self):
        settings.init()
        settings.runtime['sm'] = self.sm
        settings.runtime['downloads'] = []
        settings.runtime['sdk'] = self._android_sdk()
        go_to_screen(screens.Application)
        return self.sm

    def _android_sdk(self):
        return 1
        for line in open('/system/build.prop'):
            import re
            mth = re.search("ro.build.version.sdk=(.*)", line)
            if mth:
                return int(mth.group(1))


if __name__ == '__main__':
    AndroidBazaar().run()
