from functools import partial

from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import Screen
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from kivy.metrics import dp
from kivy.clock import Clock
from kivy.core.window import Window

from models.user import User
from helpers import go_to_screen
import settings
import screens


class SignUp(Screen):
    user_name_text = ''
    email_text = ''
    password_text = ''
    repeat_pass_text = ''

    def __init__(self, *args):
        super(SignUp, self).__init__()
        root = BoxLayout(orientation='vertical')

        body = GridLayout(cols=1, spacing=2, size_hint=(1, 1))
        body.bind(minimum_height=body.setter('height'))
        with body.canvas.before:
            self.canvas_size = self.size
            self.rect = Rectangle(pos=self.pos, size=self.canvas_size)
        container = RelativeLayout()
        title = Label(
            text="Androidbazaar",
            # font_name=logo_font_path,
            font_size=dp(42),
            pos_hint={'center_x': 0.5, 'center_y': 0.85},
        )
        container.add_widget(title)

        self.notification_label = Label(
            text='',
            size_hint=(None, None),
            size=(Window.width / 4, Window.height / 20),
            pos_hint={'center_x': 0.5, 'center_y': 0.3},
            markup=True
        )
        container.add_widget(self.notification_label)

        user_name_input = TextInput(
            hint_text='Username',
            hint_text_color=(1, 1, 1, 1),
            foreground_color=(1, 1, 1, 1),
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.7},
            opacity=0.4
        )
        user_name_input.padding_y = [user_name_input.size[1] / 3, 0]
        user_name_input.bind(text=partial(self.update_input_text, 'user_name'))
        container.add_widget(user_name_input)

        email_input = TextInput(
            hint_text='Email Address',
            hint_text_color=[1, 1, 1, 1],
            foreground_color=(1, 1, 1, 1),
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.6},
            opacity=0.4
        )
        email_input.padding_y = [email_input.size[1] / 3, 0]
        email_input.bind(text=partial(self.update_input_text, 'email'))
        container.add_widget(email_input)

        password_input = TextInput(
            hint_text='Password',
            hint_text_color=[1, 1, 1, 1],
            foreground_color=(1, 1, 1, 1),
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.5},
            password=True,
            opacity=0.4
        )
        password_input.padding_y = [password_input.size[1] / 3, 0]
        password_input.bind(text=partial(self.update_input_text, 'password'))
        container.add_widget(password_input)

        self.repeat_password_input = TextInput(
            hint_text='Repeat Password',
            hint_text_color=[1, 1, 1, 1],
            foreground_color=(1, 1, 1, 1),
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.4},
            password=True,
            opacity=0.4
        )
        self.repeat_password_input.bind(focus=self.on_repeat_password_focus)
        self.repeat_password_input.padding_y = [self.repeat_password_input.size[1] / 3, 0]
        self.repeat_password_input.bind(text=partial(self.update_input_text, 'repeat_password'))
        container.add_widget(self.repeat_password_input)

        register_button = Button(
            text='Register',
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.3},
            background_normal='',
            background_color=(1, 1, 1, 0.6),
            opacity=0.8,
            color=(1, 1, 1, 1)
        )
        register_trigger = Clock.create_trigger(self.register)
        register_button.bind(on_press=partial(register_trigger))
        container.add_widget(register_button)

        login_button = Button(
            text='Already have an account? [b]Sign In![/b]', markup=True,
            size_hint=(None, None),
            size=(Window.width, Window.height / 10),
            pos_hint={'center_x': 0.5, 'center_y': 0.05},
            background_normal='',
            background_color=(1, 1, 1, 0.3),
            opacity=0.8,
            color=(1, 1, 1, 1)
        )
        login_button.bind(on_release=partial(go_to_screen, screens.SignIn))
        container.add_widget(login_button)

        body.add_widget(container)
        root.add_widget(body)
        self.add_widget(root)
        self.has_moved = False
        Window.clearcolor = (.3, .2, .8, 1)

    def register(self, *args):
        result = User.new(self.user_name_text, self.password_text, self.email_text)
        if result:
            settings.runtime.get('sm').switch_to(screens.SignIn())

    def update_input_text(self, *args):
        referer = args[0]
        value = args[2]
        # args[0] is the referer and args[2] is the value of the textbox
        if referer == 'user_name':
            self.user_name_text = value
        elif referer == 'password':
            self.password_text = value
        elif referer == 'email':
            self.email_text = value
        else:
            self.repeat_pass_text = value

    def on_repeat_password_focus(self, instance, value):
        if value and self.has_moved is False:
            self.pos = self.pos[0], self.pos[1] + Window.height / 5
            self.has_moved = True
        elif not value and self.has_moved is True:
            self.pos = self.pos[0], self.pos[1] - Window.height / 5
            self.has_moved = False
