from kivy.uix.relativelayout import RelativeLayout


from kivy.metrics import dp
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from kivy.core.window import Window
from kivy.utils import get_color_from_hex
from kivy.clock import Clock

from custom_widgets.partials import DownloadContainer
from network.app import get_app
import settings


class Downloads(Screen):
    def __init__(self, *args):
        super(Downloads, self).__init__()

        app_view = ScrollView(
            size_hint_y=None,
            size=(Window.width, Window.height),
            scroll_y=1
        )
        app_container = RelativeLayout()

        tabbed_panel = TabbedPanel(
            size_hint=(None, None),
            size=(Window.width, Window.height),
            do_default_tab=False,
            tab_pos='top_right'
        )

        downloading_tab = TabbedPanelItem(text='Downloading...')
        downloading_scroll_view = ScrollView(
            pos_hint={'center_y': 0.8},
            scroll_y=1
        )
        downloading_grid_view = GridLayout(cols=1, spacing=2, size_hint_y=None)
        downloading_grid_view.bind(minimum_width=downloading_grid_view.setter('height'))
        for container in settings.runtime.get('downloads'):
            downloading_grid_view.add_widget(container)
        downloading_scroll_view.add_widget(downloading_grid_view)
        downloading_tab.add_widget(downloading_scroll_view)

        tabbed_panel.add_widget(downloading_tab)

        app_container.add_widget(tabbed_panel)
        app_view.add_widget(app_container)
        self.add_widget(app_view)
        self.update_event = Clock.schedule_interval(self._update_downloads_status, 5)

    def _update_downloads_status(self, *args):
        for container in settings.runtime.get('downloads'):
            container.update_status()
