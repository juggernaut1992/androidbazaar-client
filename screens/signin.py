from functools import partial

from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from kivy.metrics import dp
from kivy.core.window import Window

from models.user import User
from helpers import go_to_screen
import screens


class SignIn(Screen):
    user_name_text = ''
    password_text = ''

    def __init__(self, *args):
        super(SignIn, self).__init__()
        root = BoxLayout(orientation='vertical')

        body = GridLayout(cols=1, spacing=2, size_hint=(1, 1))
        body.bind(minimum_height=body.setter('height'))
        with body.canvas.before:
            self.canvas_size = self.size
            self.rect = Rectangle(pos=self.pos, size=self.canvas_size)

        container = RelativeLayout()
        title = Label(
            text="AndroidBazaar",
            font_size=dp(42),
            pos_hint={'center_x': 0.5, 'center_y': 0.85},
        )
        container.add_widget(title)

        user_name_input = TextInput(
            hint_text='Username',
            hint_text_color=(1, 1, 1, 1),
            foreground_color=(1, 1, 1, 1),
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.7},
            opacity=0.4
        )
        user_name_input.padding_y = [user_name_input.size[1] / 3, 0]
        user_name_input.bind(text=partial(self.update_input_text, 'user_name'))
        container.add_widget(user_name_input)

        password_input = TextInput(
            hint_text='Password',
            hint_text_color=[1, 1, 1, 1],
            padding_x=[20, 0],
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.6},
            password=True,
            opacity=0.4
        )
        password_input.padding_y = [password_input.size[1] / 3, 0]
        password_input.bind(text=partial(self.update_input_text, 'password'))
        container.add_widget(password_input)

        sign_in_button = Button(
            text='Login',
            size_hint=(None, None),
            size=(Window.width / 1.2, Window.height / 12),
            pos_hint={'center_x': 0.5, 'center_y': 0.5},
            background_normal='',
            background_color=(1, 1, 1, 0.6),
            opacity=0.8,
            color=(1, 1, 1, 1)
        )
        sign_in_button.bind(on_press=self.sign_in)
        container.add_widget(sign_in_button)
        register_button = Button(
            text="Don't have an account yet? [b]Sign Up![/b]",
            markup=True,
            size_hint=(None, None),
            size=(Window.width, Window.height / 10),
            pos_hint={'center_x': 0.5, 'center_y': 0.05},
            background_normal='',
            background_color=(1, 1, 1, 0.3),
            opacity=0.8,
            color=(1, 1, 1, 1)
        )
        register_button.bind(on_press=partial(go_to_screen, screens.SignUp))
        container.add_widget(register_button)

        body.add_widget(container)
        root.add_widget(body)
        self.add_widget(root)

    # noinspection PyUnusedLocal
    def sign_in(self, *args):
        User.get_me(self.user_name_text, self.password_text)

    def update_input_text(self, *args):
        # args[0] is the referer and args[2] is the value of the textbox
        if args[0] == 'user_name':
            self.user_name_text = args[2]
        else:
            self.password_text = args[2]
