# -*- coding: utf-8 -*-

from functools import partial

from kivy.graphics.vertex_instructions import Rectangle
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.button import Button
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.image import AsyncImage

from kivy.metrics import dp
from kivy.core.window import Window

from models.application import Application
from custom_widgets.partials import AppContainer
from helpers import go_to_screen
import screens


class Index(Screen):
    def __init__(self, *args):
        super(Index, self).__init__()
        # body = BoxLayout(orientation='vertical')
        # body.bind(minimum_height=body.setter('height'))
        # with body.canvas.before:
        #     self.rect = Rectangle(pos=self.pos, size=self.size)

        """Top Applications"""
        top_apps_label = Label(
            text=u'تازه های برگزیده',
            color=(1, 1, 1, 0.7),
            pos_hint={'center_y': 0.95, 'center_x': 0.5},
            font_name='FreeFarsi',
            font_size=dp(20),
            rtl=True,
            underline=True,
            size_hint=(None, None)
        )
        top_apps_label.halign = 'right'
        top_apps_label.valgin = 'top'
        top_apps_label.bind(texture_size=top_apps_label.setter('size'))

        top_apps_view = ScrollView(
            size_hint=(None, None),
            size=(Window.width, Window.height / 3),
            pos_hint={'center_y': 0.75},
            scroll_x=1
        )
        top_apps_view.bar_width = '1dp'
        top_apps = GridLayout(rows=1, spacing=10, size_hint_x=None)
        top_apps.bind(minimum_width=top_apps.setter('width'))
        for i in range(10):
            top_apps.add_widget(
                AppContainer(
                    1,
                    u'تلگرام فارسی',
                    "http://192.168.1.60:8686/storage/ok.png",
                    u'۲۰۰۰ تومان'
                )
            )

        top_apps_view.add_widget(top_apps)

        """Most Viewed Applications"""
        most_viewed_label = Label(
            text=u'برنامه های پربازدید',
            color=(1, 1, 1, 0.7),
            pos_hint={'center_y': 0.55, 'center_x': 0.5},
            font_name='FreeFarsi',
            font_size=dp(20),
            rtl=True,
            underline=True
        )
        most_viewed_label.halign = 'right'
        most_viewed_label.valgin = 'top'
        most_viewed_label.bind(texture_size=most_viewed_label.setter('size'))

        most_viewed_view = ScrollView(
            size_hint=(None, None),
            size=(Window.width, Window.height / 3),
            pos_hint={'center_y': 0.35},
            scroll_x=1
        )
        most_viewed_view.bar_width = '1dp'

        most_viewed_apps = GridLayout(rows=1, spacing=10, size_hint_x=None)
        most_viewed_apps.bind(minimum_width=most_viewed_apps.setter('width'))
        for i in range(10):
            app = AppContainer(1, u'تلگرام فارسی', "http://192.168.1.60:8686/storage/ok.png", u'۲۰۰۰ تومان')
            app.bind(on_touch_down=self._on_touch_down)
            most_viewed_apps.add_widget(app)

        most_viewed_view.add_widget(most_viewed_apps)

        self.add_widget(top_apps_label)
        self.add_widget(top_apps_view)

        self.add_widget(most_viewed_label)
        self.add_widget(most_viewed_view)

    def _on_touch_down(self, *args):
        if args[0].collide_point(*args[1].pos):
            go_to_screen(screens.Application, args[0].app_uid)
        return super(Index, self).on_touch_down(args[1])
