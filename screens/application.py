# -*- coding: utf-8 -*-
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle
from kivy.uix.gridlayout import GridLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.screenmanager import Screen
from kivy.uix.scrollview import ScrollView
from kivy.uix.image import AsyncImage
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelItem
from kivy.core.window import Window
from kivy.utils import get_color_from_hex

from custom_widgets.partials import CommentContainer
from helpers import go_to_screen, store_download, density_value
from custom_widgets.partials import DownloadContainer
from models.download import DownloadTask
import screens


Window.clearcolor = get_color_from_hex('#26A7B0')


class Application(Screen):
    def __init__(self, *args):
        super(Application, self).__init__()
        with self.canvas.before:
            Color(0, 0, 0, 0.1)
            self.rect = Rectangle(pos=self.pos, size=self.size)

        app_view = ScrollView(
            size_hint_y=None,
            size=(Window.width, Window.height),
            scroll_y=1
        )
        app_container = RelativeLayout()

        logo = AsyncImage(
            source='http://illc.ir/storage/why_python',
            size_hint=(None, None),
            size=(Window.width / 2, Window.height / 2),
            allow_stretch=False,
            pos_hint={'center_x': 0.8, 'center_y': 0.8}
        )

        name = Label(
            text='تلگرام عربی',
            pos_hint={'center_x': 0.1, 'center_y': 0.9},
            color=(1, 1, 1, 0.9),
            font_size=density_value(15),
            halign='right',
            valign='middle',
            font_name='Nazanin',
            rtl=True,
        )
        name.bind(size=name.setter('text_size'))

        author_category = Label(
            text='گروه نرم افزاری سگ',
            pos_hint={'center_x': 0.1, 'center_y': 0.80},
            color=(1, 1, 1, 0.8),
            font_size=density_value(40),
            halign='right',
            valign='middle',
            font_name='Sans',
            rtl=True,
        )
        author_category.bind(size=author_category.setter('text_size'))

        size = Label(
            text='۸.۸ مگابایت',
            pos_hint={'center_x': 0.1, 'center_y': 0.75},
            color=(1, 1, 1, 0.8),
            font_size=density_value(40),
            halign='right',
            valign='middle',
            font_name='Sans',
            rtl=True,
        )
        size.bind(size=size.setter('text_size'))

        version = Label(
            text='نسخه ۱.۴',
            pos_hint={'center_x': 0.1, 'center_y': 0.70},
            color=(1, 1, 1, 0.8),
            font_size=density_value(40),
            halign='right',
            valign='middle',
            font_name='Sans',
            rtl=True,
        )
        version.bind(size=version.setter('text_size'))

        add_to_fav = Button(
            text=u'افزودن به علاقه مندی ها',
            pos_hint={'center_x': 0.3, 'center_y': 0.6},
            background_normal='',
            background_color=get_color_from_hex('#616161'),
            size_hint=(None, None),
            size=(Window.width / 2.5, Window.height / 15),
            font_size=density_value(40),
            font_name='Sans',
            rtl=True,
        )

        download = Button(
            text=u'دریافت (۲۰۰۰ تومان)',
            pos_hint={'center_x': 0.75, 'center_y': 0.6},
            background_normal='',
            background_color=get_color_from_hex('#00a558'),
            size_hint=(None, None),
            size=(Window.width / 2.5, Window.height / 15),
            font_size=density_value(40),
            font_name='Sans',
            rtl=True,
        )
        download.bind(on_press=self._on_download_request)

        tabbed_panel = TabbedPanel(
            size_hint=(None, None),
            size=(Window.width, Window.height * 0.5),
            do_default_tab=False,
            tab_pos='top_right'
        )

        description_tab = TabbedPanelItem(
            background_normal='',
            background_color=get_color_from_hex('#136F76'),
            text='توضیحات',
            font_name='Sans',
            rtl=True,
            font_size=density_value(40)
        )

        description = Label(
            text=u"دی گو‍زی",
            font_name='Sans',
            pos_hint={'center_x': 0.5, 'center_y': 0.4},
            color=(0, 0.517, 0.705, 1),
            font_size=density_value(14),
            size_hint=(1, None),
            rtl=True
        )
        description.bind(width=lambda *x: description.setter('text_size')(description, (description.width, None)))
        description.bind(texture_size=lambda *x: description.setter('height')(description, description.texture_size[1]))
        description_tab.add_widget(description)

        permissions_tab = TabbedPanelItem(
            background_normal='',
            background_color=get_color_from_hex('#136F76'),
            text=u'دسترسی ها',
            font_name='Sans',
            rtl=True,
            font_size=density_value(40)
        )

        permission = Label(
            text="Permission",
            pos_hint={'center_x': 0.5, 'center_y': 0.4},
            color=(0, 0.517, 0.705, 1),
            font_size=density_value(14),
            size_hint=(1, None)
        )
        permission.bind(width=lambda *x: permission.setter('text_size')(permission, (permission.width, None)))
        permission.bind(texture_size=lambda *x: permission.setter('height')(permission, permission.texture_size[1]))
        permissions_tab.add_widget(permission)

        comments_tab = TabbedPanelItem(
            background_normal='',
            background_color=get_color_from_hex('#136F76'),
            text=u'نظرات',
            font_name='Sans',
            rtl=True,
            font_size=density_value(40)
        )
        comments_scroll_view = ScrollView(
            pos_hint={'center_y': 0.8},
            scroll_y=1
        )
        comments_grid_view = GridLayout(cols=1, spacing=2, size_hint_y=None)
        comments_grid_view.bind(minimum_width=comments_grid_view.setter('height'))
        for i in range(10):
            comments_grid_view.add_widget(
                CommentContainer(
                    author=u'هاشم بیگ زاده',
                    created='دیروز',
                    text='خیلی هم عالی',
                    plus='۱۲',
                    minus='۳'
                )
            )
        comments_scroll_view.add_widget(comments_grid_view)

        comments_tab.add_widget(comments_scroll_view)

        tabbed_panel.add_widget(comments_tab)
        tabbed_panel.add_widget(permissions_tab)
        tabbed_panel.add_widget(description_tab)

        app_container.add_widget(author_category)
        app_container.add_widget(logo)
        app_container.add_widget(name)
        app_container.add_widget(size)
        app_container.add_widget(version)
        # app_container.add_widget(price)
        app_container.add_widget(download)
        app_container.add_widget(add_to_fav)
        # app_container.add_widget(description)
        app_container.add_widget(tabbed_panel)
        app_view.add_widget(app_container)
        self.add_widget(app_view)

    def _on_download_request(self, *args):
        self.task = DownloadTask(
            'http://192.168.1.60:8686/storage/KingoRoot.apk',
            {},
            'Telegram Farsi',
            'telegram.package.name'
        )
        download_id = self.task.start()
        self.task.android_download_uid = download_id
        download_container = DownloadContainer(self.task)
        store_download(download_container)
        go_to_screen(screens.Downloads)
