from kivy.network.urlrequest import UrlRequest
from variables import API_URL


def get_app(app_uid, callback):
    UrlRequest(
        '{}/api/applications/{}'.format(API_URL, app_uid),
        on_success=callback
    )
