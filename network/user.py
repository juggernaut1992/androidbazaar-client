import json

from kivy.network.urlrequest import UrlRequest

from variables import API_URL


def register(username, password, email):
    params = {
        'user_name': username,
        'email_address': email,
        'password': password
    }
    UrlRequest(
        url='{}/api/users'.format(API_URL),
        req_body=json.dumps(params),
        method='POST',
        req_headers={'Content-Type': 'application/json'}
    )


def login(username, password, callback):
    params = {
        'user_name': username,
        'password': password
    }
    UrlRequest(
        url='{}/api/users/request_session'.format(API_URL),
        req_body=json.dumps(params),
        method='POST',
        req_headers={'Content-Type': 'application/json'},
        on_success=callback
    )
